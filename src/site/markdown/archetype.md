##构建Maven脚手架
1.清除eclipse工程编译产生的文件`mvn clean`
  
2.在主目录下执行命令`mvn archetype:create-from-project`

3.进入目录/smart-boot/target/generated-sources/archetype/src/main/resources/archetype-resources执行clean.sh，结束后删除该文件

4.`/smart-boot/target/generated-sources/archetype`目录下的内容便是一个Maven脚手架工程。  

已生成现成的archetype，参见[smartboot-archetype](https://git.oschina.net/smartboot/smartboot-archetype)

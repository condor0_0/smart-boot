package org.smartboot.service.test;

import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@Rollback(false)
@Transactional(
	transactionManager = "transactionManager")
@ContextConfiguration(
	locations = { "classpath*:service-config.xml" }, classes = AbstractUnitTest.class)
@SpringBootTest
@SpringBootApplication(
	scanBasePackages = { "org.smartboot" })
public abstract class AbstractUnitTest {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(AbstractUnitTest.class, args);
	}
}